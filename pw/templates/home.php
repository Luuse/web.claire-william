<?php

  $currentPage = 'projets';

  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('agenda.php');

  include('projets.php');

  include('inc/nav.php');
  include('inc/foot.php');
