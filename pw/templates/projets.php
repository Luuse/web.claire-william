<?php
  $projets = $pages->find('template=projet');
?>

<section id="projets" class="content" data-simplebar data-simplebar-auto-hide="false">

  <div class="grid">
    <article class="grid_sizer"> </article>

      <?php foreach ($projets as $projet): ?>
        <?php

          $categorieItems = [];
          $objets = $projet->objets;
          $materiaux = $projet->materiaux;
          $techniques = $projet->techniques;
          $collaborations = $projet->collaborations;
          $annee = $projet->annee;
          $imageFirst = $projet->images->first;
          if ($imageFirst->ext != 'gif') {
            $imageThumb = $imageFirst->size('400', '0');
          } else {
            $imageThumb = $imageFirst;
          }

          $categorieItems = getCategories($objets, $categorieItems);
          $categorieItems = getCategories($materiaux, $categorieItems);
          $categorieItems = getCategories($techniques, $categorieItems);
          $categorieItems = getCategories($collaborations, $categorieItems);
          $categorieItems = getCategories($annee, $categorieItems);

        ?>
        <article data-id="<?= $projet->id ?>" class="projet <?php echo $projet->name; foreach($categorieItems as $item) echo ' '.$item; ?> grid_item">
          <a href="<?= $projet->url ?>">
            <div class="image">
              <img src="<?= $imageThumb->url ?>" />
            </div>
            <h1 lang="en"><?= $projet->title ?></h1>
          </a>
        </article>

      <?php endforeach ?>
  </div>
</section>
