<?php

  $currentPage = 'blog';

  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('agenda.php');

  $blog = $pages->find('template=blogPost');

?>

  <section id="blog" class="content" data-simplebar data-simplebar-auto-hide="false">
    <?php foreach ($blog as $blogPost): ?>

      <?php

        $categorieItems = [];
        $objets = $blogPost->objets;
        $materiaux = $blogPost->materiaux;
        $techniques = $blogPost->techniques;
        $collaborations = $blogPost->collaborations;
        $annee = $blogPost->annee;
        $imageFirst = $blogPost->images->first;
        // if ($imageFirst->ext != 'gif') {
        //   $imageThumb = $imageFirst->size('0', '43');
        // } else {
        //   $imageThumb = $imageFirst;
        // }
        $categorieItems = getCategories($objets, $categorieItems);
        $categorieItems = getCategories($materiaux, $categorieItems);
        $categorieItems = getCategories($techniques, $categorieItems);
        $categorieItems = getCategories($collaborations, $categorieItems);
        $categorieItems = getCategories($annee, $categorieItems);

      ?>

      <article class="blog <?php echo $blogPost->name; foreach($categorieItems as $item) echo ' '.$item; ?>">
        <h1><a href="<?= $blogPost->url ?>"><?= $blogPost->title ?></a></h1>
        <h2 class="date"><?php echo $blogPost->date ? $blogPost->date : date("j/m/y", $blogPost->created); ?></h2>
        <img class="vignette" data-src="<?= $imageFirst->url ?>"/>
      </article>

    <?php endforeach ?>

  </section>

<?php
  include('inc/nav.php');
  include('inc/foot.php');
