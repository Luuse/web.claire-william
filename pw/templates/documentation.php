<?php

  $currentPage = 'onePage';

  $previous    = $_GET['previous'];
  $previousType = $_GET['type'];
  $previous    = $pages->get('name='.$previous);

  $currentProjet = $page->name;
  $projets  = $pages->find('template=documentation');
  $blogs    = $pages->find('template=blogPost');
  $blogAssocies = [];

  foreach ($blogs as $blog) {
    foreach ($blog->documentationAssociee as $documentationAssociee) {
      if ($documentationAssociee->name == $currentProjet) {
        array_push($blogAssocies, $blog);
      }
    }
  }

  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('agenda.php');

?>

  <section id="documentation">

    <?php if (strlen($previous) > 0): ?>

      <div class="goBefore">
          <a class="<?= $previousType ?>" href="<?= $previous->url ?>">
            <span><?= $previous->title ?></span>
            <p class="titleHover">
              <img src="<?= $previous->images->first->url ?>" />
            </p>
          </a>
      </div>

    <?php endif ?>

    <article class="letexte" data-simplebar data-simplebar-auto-hide="false">
      <h1><?= $page->title ?></h1>
      <?= $page->texte ?>
    </article>
    <?php if (count($blogAssocies) > 0 || count($page->projetsAssocies) > 0 ): ?>
    <div class="goAfter">


      <?php foreach ($blogAssocies as $blog): ?>

        <a class="blog" href="<?= $blog->url ?>?previous=<?= $currentProjet ?>&amp;type=blogPost">
          <span><?= $blog->title ?></span>
          <p class="titleHover">
            <img src="<?= $blog->images->first->url ?>"/>
          </p>
        </a>

      <?php endforeach ?>

      <?php foreach ($page->projetsAssocies as $projet): ?>

        <a class="projet" href="<?= $projet->url ?>?previous=<?= $currentProjet ?>&amp;type=documentation">
          <span><?= $projet->title ?><br></span>
          <p class="titleHover">
            <img src="<?= $projet->images->first->url ?>" />
          </p>
        </a>

      <?php endforeach ?>

    </div>

    <?php endif ?>

  </section>

<?php

include('inc/nav.php');
include('inc/foot.php');
