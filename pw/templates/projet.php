<?php

  $currentPage = 'onePage';
  $previous    = $_GET['previous'];
  $previous    = $pages->get('name='.$previous);
  $previousType = $_GET['type'];

  $en  = $languages->get("en");
  $page->setOutputFormatting(false);

  $texte = $page->texte;
  $texteEN = $page->texte->getLanguageValue($en);

  $currentProjet = $page->name;
  $docs  = $pages->find('template=documentation');
  $blogs = $pages->find('template=blogPost');
  $docsAssocies = [];
  $blogAssocies = [];

  foreach ($docs as $doc) {
    foreach ($doc->projetsAssocies as $projetsAssocie) {
      if ($projetsAssocie->name == $currentProjet) {
        array_push($docsAssocies, $doc);
      }
    }
  }

  foreach ($blogs as $blog) {
    foreach ($blog->projetsAssocies as $projetsAssocie) {
      if ($projetsAssocie->name == $currentProjet) {
        array_push($blogAssocies, $blog);
      }
    }
  }

  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('agenda.php');

?>

  <section id="projet" class="projet">

    <?php if (strlen($texteEN) > 0): ?>
      <ul class="language">
        <li class="fr current">Fr</li>
        <li class="en">En</li>
      </ul>
    <?php endif ?>

    <?php if (strlen($previous) > 0): ?>

      <div class="goBefore">
          <a class="<?= $previousType ?>" href="<?= $previous->url ?>">
            <span><?= $previous->title ?></span>
            <p class="titleHover">
              <img src="<?= $previous->images->first->url ?>" />
            </p>
          </a>
      </div>

    <?php endif ?>

    <article class="letexte" data-simplebar data-simplebar-auto-hide="false">
      <h1><?= $page->title ?></h1>
      <div class="fr lang">
        <?= $texte ?>
      </div>
      <?php if (strlen($texteEN) > 0): ?>
        <div class="en lang">
          <?= $texteEN ?>
        </div>
      <?php endif ?>
    </article>
    <?php if (count($blogAssocies) > 0 || count($docsAssocies) > 0 ): ?>

    <div class="goAfter">

      <?php foreach ($blogAssocies as $blog): ?>

        <a class="blog" href="<?= $blog->url ?>?previous=<?= $currentProjet ?>&amp;type=blog"">
          <span><?= $blog->title ?></span>
          <p class="titleHover">
            <img src="<?= $blog->images->first->url ?>"/>
          </p>
        </a>

      <?php endforeach ?>

      <?php foreach ($docsAssocies as $doc): ?>

        <a class="documentation" href="<?= $doc->url ?>?previous=<?= $currentProjet ?>&amp;type=projet">
          <span><?= $doc->title ?><br></span>
          <p class="titleHover">
            <img src="<?= $doc->images->first->url ?>" />
          </p>
        </a>

      <?php endforeach ?>

    </div>
      <?php endif ?>

  </section>

<?php

include('inc/nav.php');
include('inc/foot.php');
