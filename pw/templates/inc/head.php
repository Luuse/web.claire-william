<?php if($debug == true) ini_set('display_errors', 'On'); ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Claire Williams</title>
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0,
maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>style/reset.css" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $config->urls->templates ?>style/faviconok.ico"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>style/main.css" />
    <!-- <link rel="stylesheet/less" type="text/css" href="<?php echo $config->urls->templates ?>style/main.less" /> -->
    <link rel="stylesheet/less" type="text/css" href="<?php echo $config->urls->templates?>style/responsive.css" />
    <script type="text/javascript" src="<?php echo $config->urls->templates ?>js/lib/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $config->urls->templates ?>js/lib/less.min.js"></script>
    <script type="text/javascript" src="<?php echo $config->urls->templates ?>js/lib/masonry.pkgd.min.js"></script>
    <noscript>
      <style>
        [data-simplebar] {
          overflow: auto;
        }
      </style>
    </noscript>
  </head>
  <body>
