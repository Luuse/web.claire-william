<?php

  $home         = $pages->get('template=home');
  $categories   = $pages->find('template=categorie, sort=sort');
  $currentPages = $pages->get('template='.$currentPage);
  $pageTemplate = $page->parent->template;

  $pageCategories = [];

?>

  <section id="nav" data-simplebar data-simplebar-auto-hide="false" class="<?= $currentPage ?>">
    <div id="menu">
      <ul>
        <li><h1><a href="<?= $pages->get(1)->httpUrl ?>">Claire Williams</a></h1></li>
        <?php foreach ($home->children as $thePage): ?>
          <?php
            $pageName = str_replace('-', '', $thePage->name);
            if ($pageName == 'categories') continue;
            if ($pageName == 'projets'){
              echo '<li class="'.$pageName;
              if ($currentPage == $pageName || $pageTemplate == $pageName) echo ' current ';
              echo '"><a href="' . $home->url . '">' . $thePage->title .  '</a></li>';
            } else if ($pageName == 'contact') {
              echo '<li class="'.$pageName;
              echo '"><a href="mailto:clairewiwi@gmail.com">' . $thePage->title .  '</a></li>';
            } else {
            ?>
              <li class="<?php echo $pageName; if ($currentPage == $pageName || $pageTemplate == $pageName) echo ' current'; ?>"><a class="<?= $pageName ?>Btn" href="<?= $thePage->url ?>"><?= $thePage->title ?></a></li>
            <?php } ?>
        <?php endforeach ?>
      </ul>
    </div>

    <h2></h2>

  <?php

  $categorieItems = [];

  if ($currentPage == 'onePage') {

    $objets = $page->objets;
    $materiaux = $page->materiaux;
    $techniques = $page->techniques;
    $collaborations = $page->collaborations;
    $tools = $page->tools;
    $annee = $page->annee;

    $categorieItems = getCategories($objets, $categorieItems);
    $categorieItems = getCategories($materiaux, $categorieItems);
    $categorieItems = getCategories($techniques, $categorieItems);
    $categorieItems = getCategories($collaborations, $categorieItems);
    $categorieItems = getCategories($tools, $categorieItems);
    $categorieItems = getCategories($annee, $categorieItems);

    foreach ($categorieItems as $item) {
      array_push($pageCategories, $item);
    }

  } else {

    foreach ($currentPages->children as $thePage) {

      $objets = $thePage->objets;
      $materiaux = $thePage->materiaux;
      $techniques = $thePage->techniques;
      $collaborations = $thePage->collaborations;
      $tools = $thePage->tools;
      $annee = $thePage->annee;

      $categorieItems = getCategories($objets, $categorieItems);
      $categorieItems = getCategories($materiaux, $categorieItems);
      $categorieItems = getCategories($techniques, $categorieItems);
      $categorieItems = getCategories($collaborations, $categorieItems);
      $categorieItems = getCategories($tools, $categorieItems);
      $categorieItems = getCategories($annee, $categorieItems);

      foreach ($categorieItems as $item) {
        array_push($pageCategories, $item);
      }

    }
  }

    $categorieItems  = array_unique($categorieItems);

    ?>

    <div id="categories<?= $currentPage ?>" class="categories">

      <?php foreach ($categories as $categorie): ?>
        <ul class="categorie <?= $categorie->name ?>">
          <li class="title"><h1><?= $categorie->title ?></h1></li>

          <?php
            foreach ($categorie->children as $item){
              if (in_array($item->name, $pageCategories)) {
                echo '<li class="' . $item->name . '">' . $item->title . '</li>';
              }
            }
          ?>

        </ul>

      <?php endforeach ?>
    </div>
    <?php if ($currentPage == 'apropos' || $currentPage == 'onePage'): ?>

      <div id="licencescredits" class="licencescredits">
        <?php echo $currentPage == 'apropos' ? '<img class="logoFWB" alt="Fédération Wallonie Bruxelles" src="http://www.culture.be/fileadmin/sites/culture/upload/culture_super_editor/culture_editor/images/Logos_FWB/logo_noir.jpg"/><br>' : ''; ?>

          This work is licensed under <a href="http://artlibre.org/licence/lal/en/">Free Art License</a> and&nbsp;<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>
          <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>
        <?php echo $currentPage == 'apropos' ? '<p>Design &amp; développement: <a href="http://www.luuse.io/">Luuse</a></p>' : ''; ?>
      </div>

    <?php endif; ?>

</section>
