function classContent(article){
  var filter = $('.categories').find('li');
  filter.click(function(){
    var theClass = $(this).attr('class').split(' ')[0];
    if (!$(this).hasClass('beyond')){
      $(this).addClass('beyond');
      article.removeClass('beyond').addClass('behind');
      $('.content').find('.'+theClass).addClass('beyond');
    } else {
      $(this).removeClass('beyond');
      if (!filter.hasClass('beyond')) {
        article.removeClass('beyond').removeClass('behind');
      } else{
        $('#projets').find('.'+theClass).removeClass('beyond').addClass('behind');
      }
    }
  })
}

function ImgLegend(article){
  var img = article.find("img");
    img.each(function() {
      var legend = $(this).attr("alt");

    $(this).wrap("<figure></figure>");
    if (legend.length != 0) {
      $(this).after("<figcaption>" + legend + "</figcaption>");
    }
  });
}

function hideClaire(){
  $('.letexte').mouseover(function(){
    $('.claire').hide();
  });
  $('.letexte').mouseout(function(){
    $('.claire').show();
  });
}

function loadIframe(theSection){
  var search = 'iframe';
  var i = 0;
  theSection.find('p').each(function() {
    var iframe = $(this).text().match(search);
    if (iframe) {
      var searchIframe = ['&lt;', '&gt;']
      var replaceIframe = ['<', '>'];
      var content = $(this).text();
      for (var i = 0; i < searchIframe.length; i++) {
        content = content.replace(searchIframe[i], replaceIframe[i]);
      }
      $(this).html(content);
      var iframe = $(this).find('iframe');
      iframe.next('p').remove();
    }
  })
}

function hoveredOffscreen(el, elToAct){
  el.each(function(){
    var thisOffset = $(this).offset().top;
    var windowH = $(window).height();
    if (thisOffset > windowH) {
      elToAct.addClass('bottom');
      if (elToAct.attr('id') == 'blog') {
        elToAct.css({'box-shadow': 'white 0px -40px 40px -40px inset'});
      } else {
        if (elToAct.hasClass('top')) {
          elToAct.css({'box-shadow': 'blue 0px 40px 40px -40px inset'});
          elToAct.children('.simplebar-scroll-content').css({'box-shadow': 'blue 0px -40px 40px -40px inset'});
        } else {
          elToAct.css({'box-shadow': 'blue 0px -40px 40px -40px inset'});
        }
      }
    } else if (thisOffset < 0) {
      elToAct.addClass('top');
      if (elToAct.attr('id') == 'blog') {
        elToAct.css({'box-shadow': 'white 0px 40px 40px -40px inset'});
      } else {
        elToAct.css({'box-shadow': 'blue 0px 40px 40px -40px inset'});
        if (elToAct.hasClass('bottom')) {
          elToAct.children('.simplebar-scroll-content').css({'box-shadow': 'blue 0px -40px 40px -40px inset'});
        }
      }
    }
  })
}

function hoverProjet(article, nav){
  article.hover(function(){
    var theClasses = $(this).attr('class').split(' ');
    for (var i = 0; i < theClasses.length; i++) {
      var sameAsHover = nav.find('.categorie').children('li').filter('.' + theClasses[i]);
      sameAsHover.addClass('hovered');
      hoveredOffscreen(sameAsHover, nav);
    }
  }, function(){
      nav.find('.categorie').children('li').removeClass('hovered');
      nav.removeClass('top bottom');
      nav.find('.simplebar-scroll-content').css({'box-shadow': 'none'});
      $('#nav').css({'box-shadow': 'none'});
  })
}


function listStates(nav){
  var article = $('.content').find('article');
  var navEl = nav.find('.categorie').children('li');
  navEl.click(function(){
    var theClass = $(this).attr('class').split(' ')[0];
    if (theClass != 'title') {
      if (!$(this).hasClass('clicked')) {
        articleState(theClass, article, 'clicked');
        $(this).addClass('clicked');
      } else {
        $(this).removeClass('clicked');
        articleState(theClass, article, 'unclicked');
      }
    }
  })

  navEl.hover(function(){
    var theClass = $(this).attr('class').split(' ')[0];
    if (theClass != 'title') {
      articleState(theClass, article, 'hovered');
    }
  }, function(){
    var theClass = $(this).attr('class').split(' ')[0];
      article.removeClass('top bottom');
      article.find('.simplebar-scroll-content').css({'box-shadow': 'none'});
      articleState(theClass, article, 'unhovered');
  })
}

function articleState(theClass, article, state){
  article.each(function(){
    if ($(this).hasClass(theClass) && state == 'clicked'){
      $(this).addClass('behind');
      hoveredOffscreen($(this), $('.content'));
    } else if ($(this).hasClass(theClass) && state == 'unclicked'){
      $(this).removeClass('behind');
      $('.content').css({'box-shadow': 'none'});
    } else if ($(this).hasClass(theClass) && state == 'hovered'){
      $(this).addClass('behindHover');
      hoveredOffscreen($(this), $('.content'));
    } else if ($(this).hasClass(theClass) && state == 'unhovered'){
      $(this).removeClass('behindHover');
      $('.content').css({'box-shadow': 'none'});
      $('.content .simplebar-scroll-content').css({'box-shadow': 'none'});
    }
  })
}

function removeCategorie(nav){
  var categorie = nav.find('.categorie');
  categorie.each(function(){
    if ($(this).children('li').length == 1) {
      $(this).hide();
    }
  })
}

function goGoMaso(){
  $('.grid').masonry({
    // set itemSelector so .grid-sizer is not used in layout
    itemSelector: '.grid_item',
    // use element for option
    columnWidth: '.grid_sizer',
    percentPosition: true
  })
}

function masoFallback(article){
  if (article.last().css('top') == '0px') {
    goGoMaso();
  }
}

function hoverBlogTitle(blog){

  $('.vignette').hide();

  blog.find('article').mouseenter(function(){
    srcImg($(this));
    $(this).children('.vignette').show();
  }).mouseleave(function(){
    $(this).children('.vignette').hide();
  });
}

function hoverGoAfter(){
  $('.titleHover').hide();

  $('a').mouseover(function(){
    $(this).children('p').show();
  });
  $('a').mouseout(function(){
    $(this).children('.titleHover').hide();
  });
}


function escalier(nav, tagList) {
  var size2 = tagList.css('font-size');
  console.log(size2);
  var size = parseInt(size2.substr(0, size2.length - 2))+2;
  tagList.each(function(){
    var li = $(this);
    var chaine = li.text().split('');
    var count = 0;

    li.text('  ');

    for (var i = 0, len = chaine.length; i < len; i++) {

      if(i > (len - 10)){
        $('<span/>', {
          class: 'mono',
          text: chaine[i],
          style: 'font-size:' + (size - count) + 'px; padding-top:'+(size - count),
        }).appendTo(li);
        if (tagList.parent().attr('class') == 'title') {
          $('.title h1 .mono').css({'font-size': '20px'});
        }
        if(i == 0){
          widthMono = $('span.mono').width();
          if (widthMono == 0) {
            widthMono = 17;
          }
          $('span.mono').css({'width': widthMono});
        }
        count = count + 1;
      }else{
        $('<span/>', {
          class: 'mono',
          text: chaine[i],
          style: 'font-size:' + (size - count) + 'px',
        }).appendTo(li);
      }
    }
    if (tagList.parent().attr('class') != 'title') {
      $('<span/>', {
        class: 'mono',
        text: ' ',
        style: 'text-decoration: none; font-size:' + (size - count) + 'px',
      }).appendTo(li);
    }
  })
}

function srcImg(el){
  var img = el.find('img');
  var imgSrc = img.attr('data-src');
  img.attr('src', imgSrc);
}

function openAgendaOnLoad(actu){
  var hash = window.location.hash;
  if (hash) {
    var agendaItem = actu.find('ul.' + hash.substr(1));
    actu.css({'visibility': 'visible'});
    actu.children('.simplebar-track').show();
    new SimpleBar(document.getElementById('actu'));
    agendaMargin(actu);
    agendaItem.addClass('open');
    agendaItem.find('p, img').show();
    srcImg(agendaItem);
    var styleAttr = agendaItem.attr('style');
    agendaItem.attr('data-style', styleAttr);
    agendaItem.removeAttr('style');
    openAgendaItem(actu);
  }
}

function openAgenda(nav, actu){
  var agendaBtn = nav.find('.agendaBtn');
  agendaBtn.click(function(event){
    event.preventDefault();
    actu.css({'visibility': 'visible'});
    actu.children('.simplebar-track').show();
    agendaMargin(actu);
    openAgendaItem(actu);
    new SimpleBar(document.getElementById('actu'));
  })
}

function closeAgenda(actu){

  actu.find('h1').click(function(){
    actu.css({'visibility': 'hidden'});
    actu.children('.simplebar-track').hide();
    window.location.hash = '';
  })
}

function agendaMargin(actu){
  var i = 1;
  var j = 28;
  var windowW = $(window).width();
  var el = actu.find('ul');
  el.each(function(){
    $(this).css({
      'margin-left': i*30,
      'font-size': j
    });
    if (i*30 < windowW/1.5) {
    i++

  } else {
    i--
  }
    if (j > 0 && i%2 == 0) {
      j--
    } else if (j == 0){
      $(this).hide();
    }
  })
}

function openAgendaItem(actu){
  actu.find('ul').click(function(){
    if (!$(this).hasClass('open')) {
      var title = $(this).attr('class').split(' ')[0];
      srcImg($(this));
      window.location.hash = title;
      $(this).addClass('open');
      $(this).find('p, img').show();
      var styleAttr = $(this).attr('style');
      $(this).attr('data-style', styleAttr);
      $(this).removeAttr('style');
      // $('.down').css({'display':'none'});
      // $('.up').css({'display':'block'});
    } else {
      window.location.hash = '';
      $(this).removeClass('open');
      $(this).find('p, img').hide();
      var styleAttr = $(this).attr('data-style');
      $(this).attr('style', styleAttr);
      $(this).removeAttr('data-style');
    }
  });
}


function changeLang(article){
  $('ul.language').children('li').click(function(){
    var theClass = $(this).attr('class');
    $('ul.language').removeClass('li');
    $('ul.language').children('li').removeClass('current');
    $(this).addClass('current');
    article.find('.lang').hide();
    article.find('.lang.' + theClass).show();
  })
}

function whiteSpaceHeight(nav){
  nav.find('span').each(function(){
    if ($(this).text() == ' ' && $(this).next().length > 0){
      $(this).height($(this).next().height());
      $(this).css({'vertical-align': 'bottom'});
    }
  })
}

function imgOriginalSize(article){
  article.find('img').each(function(){
    var src = $(this).attr('src');
    var srcWithoutExt = src.split(/\.[^/.]+$/)[0];
    var fileExtension = src.substring($(this).attr('src').lastIndexOf('.') + 1);
    if (srcWithoutExt.substr(-2) == 'is') {
      $(this).attr('src', srcWithoutExt.replace(/\.[^/.]+$/, "") + '.' + fileExtension)
    }
  })
}

function resizeImgOnClick(article){
  article.find('img').click(function(){
    if ($(this).hasClass('big')) {
      $(this).removeClass('big');
    } else {
      $(this).addClass('big');
    }
  })
}

function removeLegendArrow(article) {
  article.find('figCaption').each(function(){
    if ($(this).text().length == 0) {
      $(this).addClass('noBefore');
    }
  })
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function docListTitre(section, freq) {
  var h1Article = $(section).find('h1');
  h1Article.each(function(){
    var contH1 = $(this).text().split('');
    $(this).html('');
    for (var i = 0, len = contH1.length; i < len; i++) {
      var destin = getRandomInt(freq);

      if (destin == 1) {
        $(this).append('<span>'+contH1[i]+'</span>');
      }else{
        $(this).append(contH1[i].toString());
      }
    }
  })
}

function rndm(min, max){
    var npx = Math.random() < 0.5 ? -1 : 1;
    var x = Math.floor((Math.random() * max) + min);
    var x = x * npx;
    return x;
}

function addPreToCode(){
  $('code').each(function(){
    if ($(this).parent('pre').length == 0) {
      var content = $(this).text();
      $(this).wrap('<pre></pre>');
    }
  })
}

function navTop(nav) {
  var title = nav.find('h1');
  title.children('a').click(function(e){
    e.preventDefault();
  })
  title.click(function(){
    $(this).parents('#nav').toggleClass('mobileClicked');
  })
}
