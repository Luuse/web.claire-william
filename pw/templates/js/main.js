$(document).ready(function() {

  var nav = $('#nav');
  var articles = $('.content').find('article');
  var articleProjets = $('#projets').find('article');
  var articleBlog = $('#blogPost').find('article');
  var documentations = $('#docs');
  var documentation = $('#documentation');
  var articleProjet = $('#projet, #documentation').find('article');
  var articleDocs = $('#docs').find('article');
  var apropos = $('#apropos').find('article');
  var actu = $('#actu');
  var blog = $('#blog');

  hoverProjet(articles, nav);

  hoverGoAfter();

  openAgenda(nav, actu);
  closeAgenda(actu);
  removeCategorie(nav);
  listStates(nav);

  if (apropos.length > 0) {
    changeLang(apropos);
    hideClaire(apropos);
  }

  if (documentations.length != 0) {
    docListTitre('#docs article', 30);
  }

  if (documentation.length != 0) {
    docListTitre('section#documentation', 10);
    ImgLegend($('#documentation article'));
    loadIframe(documentation);
  }

  if (articleProjet.length != 0) {
    removeLegendArrow(articleProjet);
    resizeImgOnClick(articleProjet);
    imgOriginalSize(articleProjet);
    changeLang(articleProjet);
    ImgLegend($('#projet article'));
    loadIframe($('#projet article'));
  }

  if (articleBlog.length != 0) {
    removeLegendArrow(articleBlog);
    imgOriginalSize(articleBlog);
    ImgLegend(articleBlog);
    loadIframe(articleBlog);
  }

  if (blog.length != 0) {
    hoverBlogTitle(blog);
  }

  addPreToCode();


    if ($(window).width() < 600) {
      navTop(nav);
    } else if ($(window).width() >= 600){
      $('head').append('<script type="text/javascript" src="<?php echo $config->urls->templates ?>js/lib/simplebar.js"></script>');

    }

})

$(window).on('load', function() {
  var actu = $('#actu');
  var nav = $('#nav');
  var page = $('.content').attr('id');
  var tagListCats = nav.find('.categories ul li:not(:nth-of-type(1))');
  var tagListMenu = nav.find('#menu ul li:not(:nth-of-type(1)) a');
  var tagListTitle = nav.find('.categories ul li.title h1');
  var article = $('article');

  goGoMaso();
  setTimeout(function(){ masoFallback(article) }, 3000);
  openAgendaOnLoad(actu);
  whiteSpaceHeight(nav);
  // if (page != 'apropos') {
    escalier(nav, tagListMenu);
    escalier(nav, tagListCats);
    escalier(nav, tagListTitle);
  // }

})

$(window).on('resize', function() {

})
