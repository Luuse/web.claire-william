<?php

  $currentPage = 'apropos';

  $en  = $languages->get("en");
  $page->setOutputFormatting(false);

  $texte = $page->texte;
  $texteEN = $page->texte->getLanguageValue($en);

  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('agenda.php'); 
?>
  <section id="apropos" class="content" data-simplebar data-simplebar-auto-hide="false">


    <?php if (strlen($texteEN) > 0): ?>
      <ul class="language">
        <li class="fr current">Fr</li>
        <li class="en">En</li>
      </ul>
    <?php endif ?>

    <img class="claire" src="<?php echo $config->urls->templates ?>/style/claire-s-working.png"/>
    <article class="letexte">
      <h1><?= $page->title ?></h1>
      <div class="fr lang">
        <?= $texte ?>
      </div>
      <?php if (strlen($texteEN) > 0): ?>
        <div class="en lang">
          <?= $texteEN ?>
        </div>
      <?php endif ?>
    </article>
  </section>
<?php
  include('inc/nav.php');
  include('inc/foot.php');
?>
