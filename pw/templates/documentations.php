<?php

  $currentPage = 'documentations';


  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('agenda.php');

  $docs = $pages->find('template=documentation');

?>

  <section id="docs" class="content" data-simplebar data-simplebar-auto-hide="false">
    <div class="grid">
    <article class="grid_sizer"> </article>
    <?php foreach ($docs as $doc): ?>
      <?php

        $categorieItems = [];
        $objets = $doc->objets;
        $materiaux = $doc->materiaux;
        $techniques = $doc->techniques;
        $collaborations = $doc->collaborations;
        $annee = $doc->annee;

        $categorieItems = getCategories($objets, $categorieItems);
        $categorieItems = getCategories($materiaux, $categorieItems);
        $categorieItems = getCategories($techniques, $categorieItems);
        $categorieItems = getCategories($collaborations, $categorieItems);
        $categorieItems = getCategories($annee, $categorieItems);

      ?>
      <article class="projet <?php echo $projet->name; foreach($categorieItems as $item) echo ' '.$item; ?> grid_item">
        <a href="<?= $doc->url ?>">
          <div class="bloc-vignettes">
      	    <?php
        	    $i = 0;
        	    foreach($doc->images as $image){
        	      if ($i < 20 && $image){
          	      ?>
                  <div class="image">
          	         <img src="<?= $image->size('0', '43')->url ?>" />
                  </div>
          	      <?php
          	      $i++;

        	      }
        	    }
      	    ?>
          </div>
          <h1 lang="en"><?= $doc->title ?></h1>
        </a>
      </article>

      <?php endforeach ?>
    </div>
  </section>

<?php

  include('inc/nav.php');
  include('inc/foot.php');
