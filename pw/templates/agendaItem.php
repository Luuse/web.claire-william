<?php
  $agenda = $pages->find("template=agendaItem, sort=-created");
    foreach ($agenda as $agendaItem) {
    ?>
      <ul class="<?= $agendaItem->name ?>">
        <li><sup><?= $agendaItem->date ?></sup></li>
        <li><sup><?= $agendaItem->type ?></sup></li>
        <li class="title"><?= $agendaItem->title ?></li>
        <li><sup><?= $agendaItem->lieu ?></sup></li>
        <li class="btnPlus"><sup class="down">⇩</sup><sup class="up">⇧</sup></li>
        <li><?= $agendaItem->texte ?></li>
        <li><img data-src="<?= $agendaItem->images->first->url ?>" alt="" /></li>
      </ul>
      <br>
    <?php
    }
  ?>
