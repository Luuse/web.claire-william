<?php

  $currentPage = 'onePage';

  $previous     = $_GET['previous'];
  $previousType = $_GET['type'];
  $previous     = $pages->get('name='.$previous);

  $currentProjet = $page->name;

  include('inc/variables.php');
  include('inc/functions.php');
  include('inc/head.php');
  include('agenda.php');

?>

  <section id="blogPost">

    <?php if (strlen($previous) > 0): ?>

      <div class="goBefore">
          <a class="<?= $previousType ?>" href="<?= $previous->url ?>">
            <span><?= $previous->title ?></span>
            <p class="titleHover">
              <img src="<?= $previous->images->first->url ?>" />
            </p>
          </a>
      </div>

    <?php endif ?>

    <article class="letexte" data-simplebar data-simplebar-auto-hide="false">
      <div class="article">
        <h1><?= $page->title ?></h1>
        <h2 class="date"><?php echo $page->date ? $page->date : date("j/m/y", $page->created); ?></h2>
        <?= $page->texte ?>
        <span class="language">fr</span>
        <span class="language">en</span>
      </div>
    </article>

    <?php if (count($page->documentationAssociee) > 0 || count($page->projetsAssocies) > 0 ): ?>

    <div class="goAfter">

      <?php foreach ($page->documentationAssociee as $doc): ?>

        <a class="documentation" href="<?= $doc->url ?>?previous=<?= $currentProjet ?>&amp;type=documentation">
          <span><?= $doc->title ?><br></span>
          <p class="titleHover">
            <img src="<?= $doc->images->first->url ?>" />
          </p>
        </a>

      <?php endforeach ?>

      <?php foreach ($page->projetsAssocies as $projet): ?>

        <a class="projet" href="<?= $projet->url ?>?previous=<?= $currentProjet ?>&amp;type=documentation">
          <span><?= $projet->title ?><br></span>
          <p class="titleHover">
            <img src="<?= $projet->images->first->url ?>" />
          </p>
        </a>

      <?php endforeach ?>

    </div>

    <?php endif ?>

  </section>

<?php

include('inc/nav.php');
include('inc/foot.php');
