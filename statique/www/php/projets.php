<?php
  $projet[0] = array('souffle','pass.jpg');
  $projet[1] = array('Chants Magnétiques', 'chants.jpg');
  $projet[2] = array('E-Textile Summercamp 2017', 'summercamp.jpg');
  $projet[3] = array('An Electromagnetic Walk – Exhibition', 'BALLADENET.jpg');
  $projet[4] = array('Uncanny_Creatures', 'uncanny.jpg');
?>

<section id="projets" data-simplebar>
  <div class="grid">
  <?php
    for ($u=0; $u < 10; $u++){
      for ($i=0; $i < count($projet); $i++) {
        ?>
          <article class="<?php if($i == 3) echo 'sound'; ?> projet grid_item">
            <a href="projet.php">
              <img src="img/<?php echo $projet[$i][1]; ?>" />
              <h1><?php echo $projet[$i][0]; ?></h1>
            </a>
          </article>
        <?php
      }
    }
  ?>
  </div>
</section>
