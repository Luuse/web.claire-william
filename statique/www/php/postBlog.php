<section id="postBlog" data-simplebar>
  <div class="goBefore">
    <a class="blog" href="blog.php">
      <p class="titleHover">
        ➯
        <span>Knitted Antenna research projec<br>
        </span>
        <span>2009</span>
        <span>Electronics</span>
        <span>Textile<br></span>
        <img src="img/pass.jpg"/>
      </p>
    </a>
  </div>
  <article class="blog">
    <h1>Fractals and Indigenous<br>Textiles</h1>
      <h2>22/12/14</h2>
      <img class="vignette" src="img/blog/image1.jpg"/>
    <div class="article">
      <img src="img/blog/image0.png"/>
        <p>The idea of constructing a pattern or textile structure using recursion as a rule is something that can be seen in traditional textiles throughout the world. Theorized by Benoît Mandelbrot in 1974, fractals are a geometric object infinitely fragmented where the details can be observed at any random scale. They are created by repeating a simple process over and over in an ongoing feedback loop. Driven by recursion, fractals are images of dynamic systems. Fractals and recursion can be observed in nature as in trees, rivers, coastlines, mountains, clouds, seashells, hurricanes, etc. Fractals are widely used in biology, economy and computer science using iterative algorithms executed by computers.
        </p>
    <h3>African Fractals</h3>
      <img src="img/blog/image1.jpg"/>
        <p>We can find fractals in African crafts and architecture. They reflect a spiritual relationship between the living and the ancestors in some of the African culture. In opposite to an euclidean way of constructing a structure or pattern, African fractals appear to have indeed a mythical meaning through iterative construction. In weaving, pleating or folding and stitch dying resist for example, this could also be related to the textile technique being used which naturally influences the way you construct a pattern inside a textile grid specific to the chosen technique.
        </p>
      <img src="img/blog/image2.jpg"/>
        <p>“The traditional Fulani wedding blanket is woven primarily from camel hair. The weavers who created it say that spiritual energy is woven into the pattern and that each successive iteration shows an increase in this energy. Releasing this energy is spiritually dangerous; the weavers say that if they were to stop in the middle (where the pattern is most dense, and hence the spiritual energy is greatest) they would risk death. The engaged couple must bring the weaver food and kola nuts to keep him awake until it is finished.” Ron Eglash
        </p>
      <img src="img/blog/image3.jpg"/>
    <h3>Examples from braiding, stitch dye resist and weaving</h3>
      <img src="img/blog/image4.jpg"/>
      <img src="img/blog/image5.jpg"/>
      <img src="img/blog/image6.jpg"/>
    <h4>Sources</h4>
        <p>– Book: African Fractals modern computing and indigenous design – Ron Eglash</p>
        <p>-African Fractals:http://homepages.rpi.edu/~eglash/eglash.dir/afractal/afractal.htm</p>
    <h3>Shipibo Embroidery</h3>
        <p>The Shipibo tribe live in the Amazonian forest of Peru. They are known for their intricate embroidery that take part of shamanic rituals. We can observe that their embroidered and painted patterns are constructed with recursive space filling curve rules. These recursive patterns are created by the Shipibo women who simultaneously sing an Icaro chant. “the Shipibo can listen to a song or chant by looking at the designs – and inversely, paint a pattern by listening to a song or music”.
        </p>
      <img src="img/blog/image7.jpg"/>
        <p>«As an astonishing demonstration of this I witnessed two Shipiba paint a large ceremonial ceramic pot known as a MahuetÃ¡. The pot was nearly five feet high and had a diameter of about three feet, each of the Shipiba couldn’t see what the other was painting, yet both were whistling the same song, and when they had finished both sides of the complex geometric pattern were identical and matched each side perfectly. »</p>
    <h3>Howard G. Charing – Communion With The Infinite – The Visual Music of the Shipibo tribe of the Amazon</h3>
        <p>The shaman then wears the embroidered cloth for ayahuasca healing ceremonies where he will read the structure of the pattern to sing the visual score and follow the geometric patterns to re-balance the patients body.</p>
      <img src="img/blog/image8.jpg"/>
        <p>« A key element in this magical dialogue with the energy which permeates creation and is embedded in the Shipibo designs is the work with ayahuasca by the Shipibo shamans or muraya. In the deep ayahuasca trance, the ayahuasca reveals to the shaman the luminous geometric patterns of energy. These filaments drift towards the mouth of the shaman where it metamorphoses into a chant or Icaro. The Icaro is a conduit for the patterns of creation which then permeate the body of the shamans patient bringing harmony in the form of the geometric patterns which re-balances the patients body. The vocal range of the Shipibo shamans when they chant the Icaros is astonishing, they can range from the highest falsetto one moment to a sound which resembles a thumping pile driver, and then to a gentle soothing melodic lullaby. Speaking personally of my experience with this, is a feeling that every cell in my body is floating and embraced in a nurturing all-encompassing vibration, even the air around me is vibrating in acoustic resonance with the icaro of the maestro. The shaman knows when the healing is complete as the design is clearly distinct in the patients body. It make take a few sessions to complete this, and when completed the geometric healing designs are embedded in the patients body, this is called an Arkana. This internal patterning is deemed to be permanent and to protect a person’s spirit. »</p>
        <p>The visual music score embedded in the Shipibo textiles are very intriguing in the way that we could imagine that the patterns correspond to specific musical rules of the Shipibo culture. Other than that, the recursive pattern construction can be imagined as a way to trace a map to circulate on the textile surface in the same specific way you would construct the needle work. On an other interesting article found on dataisnature.com « The Generative Song & Sound Pattern Matrixes of the Shipibo Indians » who makes an interesting parallel between the Shipibo patterns and feed back systems «resemblance of the patterns generated by video feedback especially those systems containing symmetry breaking transformations.»</p>
        <p>As in numerous tribal textiles, these textiles express a place where patterns can symbolize or be a path leading to the spiritual world. Following the idea of these recursive patterns being a way to transmit, protect or capture good or evil energy i investigated further to see if their were connections that could be made with the use of these same patterns used for energy harvesting systems or antennas. And indeed it was very interesting to discover the use of Fractal Antennas and Rectennas in modern electronic devices and systems.</p>
      <img src="img/blog/image9.jpg"/>
      <img src="img/blog/image10.jpg"/>
        <p>A fractal antenna uses a self-similar design to maximize the length, or increase the perimeter (on inside sections or the outer structure), of material that can receive or transmit electromagnetic radiation within a given total surface area or volume. Such fractal antennas are also referred to as multilevel and space filling curves, but the key aspect lies in their repetition of a motif over two or more scale sizes, or “iterations”. For this reason, fractal antennas are very compact, multiband or wideband, and have useful applications in cellular telephone and microwave communications.</p>
    <h4>Sources</h4>
        <p>– Howard G. Charing – Communion With The Infinite – The Visual Music of the Shipibo tribe of the Amazon</p>
        <p>– Dataisnature.com – The Generative Song & Sound Pattern Matrixes of the Shipibo Indians</p>
        <p>– Fractal Antenna http://en.wikipedia.org/wiki/Fractal_antenna</p>
        <p>– L systems: http://en.wikipedia.org/wiki/L-system</p>
        <p>– Recusion: http://en.wikipedia.org/wiki/Recursion</p>

        <span class="language">fr</span>
        <span class="language">en</span>
        <span class="up"><img src="img/up.png"/></span>
    </div>
  </article>
  <div class="goAfter">
    <a class="blog"  href="blog.php">
      <p class="titleHover">
        ➯
        <span>Knitted Antenna research project<br>
        </span>
        <span>2009</span>
        <span>Electronics</span>
        <span>Textile<br></span>
        <img src="img/pass.jpg"/>
      </p>
    </a>
    <a class="blog" href="blog.php">
      <p class="titleHover">
        ➯
        <span>Binary Textiles<br></span>
        <span>2011</span>
        <span>Electronics</span>
        <span>Textile<br></span>
        <img src="img/chants.jpg"/>
      </p>
    </a>
    <a class="documentation" href="documentations.php">
      <p class="titleHover">
        ➯
        <span>Chants magnétiques<br></span>
        <span>2013</span>
        <span>Sound</span>
        <span>Textile<br></span>
        <img src="img/summercamp.jpg"/>
      </p>
    </a>
    <a class="documentation" href="documentations.php">
      <p class="titleHover">
        ➯
        <span>Souffle<br></span>
        <span>2017</span>
        <span>Electronics</span>
        <span>Textile<br></span>
        <img src="img/charlou.jpg"/>
      </p>
    </a>
  </div>
</section>
