<section id="projet" data-simplebar>
  <div class="goBefore">
    <a class="blog" href="blog.php">
      <span>Knitted Antenna research projec<br></span>
      <p class="titleHover">
        <img src="img/pass.jpg"/>
      </p>
    </a>
  </div>
  <article class="letexte">
    <h1>
      Electromagnetic Walk
    </h1>
    <p>
    An Electromagnetic Walk is an installation comprising a series of everyday objects, each one connected to a headset and a control unit.
    Manufactured using textile techniques, a fine copper mesh overlaps with the familiar objects creating antennas that can pick up electromagnetic waves present in our daily environment. Amplified and translated as such by the sound, these antennas give us access to a portion of the sound spectrum usually subtracted to the human ear.
    </p>
    <p>
    During the opening that will take place on Thursday 24 March at 18:30 – and throughout the exhibition – visitors are invited to use these strange artefacts, wearing or handling them, to walk in the street and to capture and collect the various sound sources; they will thus discover its electromagnetic environment by highlighting the extremely dense landscape of electronic tools and electromagnetic fields that surround us.
    </p>
    <p>
    Constant V is a series of small-scale installations in the window of the Constant office. These installations are sometimes accompanied by a workshop led by the artist(s) in order to focus not only on the finished product, but also on the process of creation. They give the opportunity to discover the motivations, the techniques or other aspects of a work.
    </p>
    <div class="images">
      <img src="img/pass.jpg"/>
      <h3 class="legend">Photo: Jean de la Fontaine<br>Mons 2017<br>La vérité si je ment</h3>
      <img src="img/chants.jpg" />
      <h3 class="legend">Photo: Jean de la Fontaine<br>Mons 2017<br>La vérité si je ment</h3>
      <img src="img/charlou.jpg" />
      <h3 class="legend">Photo: Jean de la Fontaine<br>Mons 2017<br>La vérité si je ment</h3>
    </div>
  </article>
  <div class="goAfter">
    <a class="blog"  href="blog.php">
      <span>Knitted Antenna research project<br></span>
      <p class="titleHover">
        <img src="img/pass.jpg"/>
      </p>
    </a>
    <a class="blog" href="blog.php">
      <span>Binary Textiles</span>
      <p class="titleHover">
        <img src="img/chants.jpg"/>
      </p>
    </a>
    <a class="documentation" href="documentations.php">
      <span>Chants magnétiques<br></span>
      <p class="titleHover">
        <img src="img/summercamp.jpg"/>
      </p>
    </a>
    <a class="documentation" href="documentations.php">
      <span>Souffle<br></span>
      <p class="titleHover">
        <img src="img/charlou.jpg"/>
      </p>
    </a>
  </div>
</section>
