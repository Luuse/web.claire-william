  <h2>An electromagnetic Walk</h2>
    <div id="categories">
        <ul class="categorie" id="projets">
          <li><h1>Objets</h1></li>
            <li class="visible">hauts parleurs</li>
            <li class="visible">antennes</li>
            <li>Coil</li>
            <li>hauts parleurs</li>
            <li>antennes</li>
            <li>Coil</li>
        </ul>
        <ul class="categorie" id="projets">
          <li><h1>Matériaux</h1></li>
          <li class="sound">Copper</li>
          <li class="antennas visible">Electronics</li>
          <li class="electromagenetic visible">Sound</li>
          <li class="chants">Neodymium magnet</li>
          <li class="sound">Copper</li>
          <li class="antennas">Electronics</li>
          <li class="electromagenetic">Sound</li>
          <li class="chants">Neodymium magnet</li>
        </ul>
        <ul class="categorie" id="projets">
          <li><h1>Techniques</h1></li>
          <li class="sound">Knitting</li>
          <li class="antennas visible">Weaving</li>
          <li class="electromagenetic">Embroidery</li>
          <li class="sound">Knitting</li>
          <li class="antennas">Weaving</li>
          <li class="electromagenetic visible">Embroidery</li>
        </ul>
        <ul class="categorie" id="projets">
          <li><h1>Collaborations</h1></li>
          <li class="sound">Alain Juppé</li>
          <li class="antennas visible">Patrick Balkany</li>
          <li class="electromagenetic">Jean-Pierre Raffarin</li>
          <li class="electromagenetic">Chritine Boutin</li>
          <li class="sound">Alain Juppé</li>
          <li class="antennas">Patrick Balkany</li>
          <li class="electromagenetic">Jean-Pierre Raffarin</li>
          <li class="electromagenetic">Chritine Boutin</li>
        </ul>
        <ul class="categorie" id="projets">
          <li><h1>Année</h1></li>
          <li class="sound">2012</li>
          <li class="sound">2013</li>
          <li class="sound">2014</li>
          <li class="sound">2015</li>
          <li class="sound">2016</li>
          <li class="sound">2017</li>
          <li class="sound">2018</li>
          <li class="sound visible">2019</li>
        </ul>

    </div>


  <div id="documentation" data-simplebar>
    <?php
    $doc[0] = array('Algorithmic Patterns', '1.png');
    $doc[1] = array('Fractals and Indigenous Textiles', '2.png');
    $doc[2] = array('Knitendo', '3.png');
    $doc[3] = array('Binary Textiles', 'cardd.jpeg');
    $doc[4] = array('Knitendo', '5.png');
    for ($y = 0; $y < 3; $y++) {

      for ($i=0; $i < count($doc); $i++) {
        ?>

          <article class="projet grid_item">
          <a href="documentations.php" >
            <img src="img/doc/<?php echo $doc[$i][1]; ?>" />
              <h1>
                <?php echo $doc[$i][0]; ?>
              </h1>
          </a>
          </article>

        <?php
      }
    }
    ?>
  </div>
