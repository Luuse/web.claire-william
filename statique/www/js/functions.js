function classContent(article){
  var filter = $('#categories').find('li');
  filter.click(function(){
    var theClass = $(this).attr('class');
    if (!$(this).hasClass('behind') && article.hasClass(theClass)){
      filter.removeClass('behind');
      $(this).addClass('behind');
      article.css({'opacity': '0.5'}).removeClass('behind');
      $('#projets').find('.'+theClass).css({'opacity': '1'}).addClass('behind');
    }
  })
}

function hoverProjet(article){
  article.hover(function(){
    var title = $(this).find('h1').text();
    $('#nav').find('h2').html(title);
  }, function(){
    $('#nav').find('h2').html(' ');
  })
}

function goGoMaso(){
  $('.grid').masonry({
    itemSelector: '.grid_item'
  });
}

// function showArticle(article){
//   $('.article').hide();
//
//   $('h1').click(function(){
//       $(this).parent().children('.article').toggle(150);
//   });
// }

function hoverBlogTitle(article){
  $('.vignette').hide();

  $('h1').mouseover(function(){
    $(this).parent().children('.vignette').show();
  });
  $('h1').mouseout(function(){
    $(this).parent().children('.vignette').hide();
  });
}

function hoverGoAfter(){
  $('.titleHover').hide();

  $('a').mouseover(function(){
    $(this).children('p').show();
  });
  $('a').mouseout(function(){
    $(this).children('.titleHover').hide();
  });
}

// $('#categories').hide();
